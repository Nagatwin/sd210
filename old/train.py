import numpy as np
from keras.utils import to_categorical
from keras.models import Sequential
from keras.layers import GRU, Dropout, Dense, Activation, CuDNNGRU, Embedding
from keras.callbacks import ModelCheckpoint, EarlyStopping
import os

os.environ["CUDA_VISIBLE_DEVICES"]="0"

import keras.utils as ku

from dataset import Dataset
from generator import Generator


dataset = Dataset.load('model')


# Remove a part of the dataset in order for it to be runnable
#dataset.dataset = dataset.dataset[:100000]


#% Hyper parameters
total_words = dataset.total_words
max_sequence_len = 100
batch_size = 128

#% Creating the model
model = Sequential()
model.add(Embedding(total_words, 10, input_length=max_sequence_len+2))
model.add(CuDNNGRU(256))
model.add(Dense(total_words, activation='softmax'))

model.compile(loss='categorical_crossentropy', optimizer='adam')


#% Fitting the model
mc = ModelCheckpoint('weights_model2.h5', save_weights_only=True, save_best_only=True, verbose=1, monitor='loss')

model.fit_generator(
  Generator(dataset, batch_size, max_sequence_len=max_sequence_len),
  epochs=50,
  callbacks=[mc],
  workers=3, 
  use_multiprocessing=False
  #class_weight <- for the loss modification to avoid overfitting on the dots
)
