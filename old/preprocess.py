from dataset import Dataset

dataset = Dataset('data/eco.json')

# Preprocessing on the dataset
dataset.remove_multiple_occurences()
dataset.remove_apostrophe()
dataset.remove_rare_words()
dataset.remove_small_sentences()

#% Transforms the sentence in a list of integers representing the chars
dataset.tokenize()

#% Saving the obtained dataset
dataset.save('model')