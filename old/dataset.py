import json
import numpy as np
from keras.utils import to_categorical
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from sklearn.model_selection import train_test_split
import re
import copy
import pandas as pd
import pickle
import os
from textblob import TextBlob
from textblob_fr import PatternTagger, PatternAnalyzer
import sys

###
# Entry class
# Elements of the dataset, containing more information than a simple text string 
###
class Entry:
    def __init__(self, id, question_id, content, polarity, subjectivity):
        # question_id: The ID of the corresponding question
        # content: The string containing the answer to this question
        # polarity: -1,1 represents if the sentence is positive or negative
        # subjectivity: 0,1 represents if the sentence is objective or subjective

        self.id = id
        self.question_id = question_id
        self.polarity = polarity
        self.subjectivity = subjectivity
        
        self.content = content.lower()
        
        # Removes all the coma in the content so it doesn't cause problem to Pandas
        self.content = self.content.replace(',', ' ').replace('\n', ' ')
  
    def __repr__(self):
        # Returns the string representation of the object, so we can print it

        return "Entry: { id: " + str(self.id) + ", question_id: " + str(self.question_id) + ", content: " + self.content + ", polarity: " + self.polarity + ", subjectivity: " + self.subjectivity + "}"

    def remove_multiple_occurences(self):
        # Remove all the multiple narrow occurences of the chars "_", "." or "-"
    
        to_remove = "_.-"
        pattern = "(?P<char>[" + re.escape(to_remove) + "])(?P=char)+"

        self.content = re.sub(pattern, r"\1", self.content).replace(".", " END ")
  
    def remove_apostrophe(self):
        # Removes all the apostrophes and replace them by their equivalent without it

        # Replace the apostrophes of iPhone
        self.content = self.content.replace("’", "'")

        self.content = self.content.replace("'", "e ")
    
    def remove_non_alphanumerical(self):
        # Removes everything that is not a number or a word

        self.content = ''.join(filter(lambda x: x.isalpha() or x.isnumeric(), self.content))    
  
    def split(self):
        # Returns the words of the answer in a list

        return self.content.split()
    
    def getSequence(self, tokenizer):
        # Returns the sequence of number associated with our sentence
        # tokenizer: A given Tokenizer, should be dataset.tokenizer

        return tokenizer.texts_to_sequences([self.content])[0]
    
    def getSubSequences(self, tokenizer, max_sequence_len):
        # Returns all the subsequences of a given length from the Entry
        # tokenizer: A given Tokenizer, should be dataset.tokenizer
        # max_sequence_len: the length of our entry

        token_list = self.getSequence(tokenizer)
        sub_sequences = []

        for i in range(1, min(max_sequence_len+1, len(token_list))):
            if i <= max_sequence_len:
                sub_sequences.append(token_list[:i+1])
            else:
                sub_sequences.append(token_list[i-max_sequence_len:i+1])
        
        return pad_sequences(sub_sequences, maxlen=max_sequence_len, padding="pre")
    
    def getRaw(self):
        # Return the raw Entry so it is compatible with Pandas

        return { 'id': self.id, 'question_id': self.question_id, 'content': self.content, 'polarity': self.polarity, 'subjectivity': self.subjectivity }

###
# Dataset class
# Containing the dataset and all the function related to its processing
###
class Dataset:
    def __init__(self, dataset_name, load=True):
        # Opens the dataset and save it as an attribute
        # dataset_name: The file path to the dataset

        self.dataset = []
        self.questions = {}
        self.tokenizer = None
        self.total_words = None

        if load:
            with open(dataset_name, 'r', encoding='utf-8') as file:
                self.data = json.loads(file.read())
            
            self.retrieve()


    def retrieve(self):
        # Retrieve the answers in the dataset and stores them into the "dataset" attribute

        id = 1
        # setup toolbar
        sys.stdout.write("[%s]" % (" " * 100))
        sys.stdout.flush()
        sys.stdout.write("\b" * (101))

        current_pourcent = 0

        for i in range(len(self.data)): # answer nb i
            for k in range(len(self.data[i]['responses'])): # question nb k
                question = self.data[i]['responses'][k]
                question_id = int(question['questionId'])

                content = None

                # Question not answered
                if question['value'] is None:
                    continue

                # If it was a MCQ and the user chose "other"
                if '{' in question['value']:
                    spl = question['value'].split('"other":')

                    if len(spl) > 1 and spl[1].split('}')[0] != "null":
                        content = question['formattedValue']
                    else:
                        continue
                else:
                    content = question['formattedValue']

                if content is None:
                    continue
                
                entry = Entry(id, question_id, content, *self.analyze_sentiments(content))

                # Avoiding "NAN" value in the resulting CSV
                if len(entry.content) == 0 or entry.content == 'n/a':
                    continue
            
                self.questions[question_id] = question['questionTitle']

                self.dataset.append(entry)
                id += 1
            

            # Loading bar
            if np.floor(100 * (i / len(self.data))) != current_pourcent:
                sys.stdout.write("-")
                sys.stdout.flush()
                current_pourcent += 1
        
        sys.stdout.write("-")
        sys.stdout.flush()

        self.data = None


    def remove_multiple_occurences(self):
        # Removes multiple occurences of ., _ and - in order to avoid the overfitting over those points
        # Also replaces all the . by " END "

        for i in range(len(self.dataset)):
            self.dataset[i].remove_multiple_occurences()


    def remove_apostrophe(self):
        # Removes all the ' characters and replace them by "e "
        # The Tokenizer of Keras traditionnaly just removes the ', creating senseless words
        
        for i in range(len(self.dataset)):
            self.dataset[i].remove_apostrophe()


    def remove_rare_words(self, threshold = 0.001):
        # Remove all the words that appear rarely

        word_count = {}

        for data in self.dataset:
            for word in data.split():
                word_count[word] = word_count.get(word, 0) + 1
        
        # Calculating the thresold
        if threshold < 1:
            max_number = word_count[max(word_count, key=word_count.get)]

            threshold = threshold * max_number
        
        # Recreating the sentences
        new_dataset = []

        for data in self.dataset:
            new_data = []

            for word in data.split():
                if word_count[word] >= threshold:
                    new_data.append(word)

            if len(new_data) > 0:
                new_dataset.append(Entry(data.id, data.question_id, " ".join(new_data), data.polarity, data.subjectivity))
        
        self.dataset = new_dataset


    def remove_small_sentences(self, threshold = 50):
        # Removes all the sentences that have fewer than `threshold` characters in it

        self.dataset = list(filter(lambda x: len(x.content) > threshold, self.dataset))


    def remove_non_alphanumerical(self):
        # Removes everything that is not a number or a word

        for i in range(len(self.dataset)):
            self.dataset[i].remove_non_alphanumerical()
    

    def analyze_sentiments(self, text):
        # Calculate the coefficients related to the sentiment analysis
        # cf https://textblob.readthedocs.io/en/dev/quickstart.html

        try:
            blob = TextBlob(text, pos_tagger=PatternTagger(), analyzer=PatternAnalyzer())
            return blob.sentiment
        except:
            return (0,0)


    def tokenize(self):
        # Create a tokenizer out of the dataset
        # This function has to be called after doing the preprocessing operations

        self.tokenizer = Tokenizer()
        self.tokenizer.fit_on_texts(self.getContent())

        self.total_words = len(self.tokenizer.word_index) + 1

        return self.tokenizer


    def getBatch(self, batch_size):
        # Retrieves a random batch out of the whole dataset
        
        idx = np.random.randint(0, len(self.dataset), batch_size)

        return np.array(self.dataset)[idx]


    def getContent(self):
        # Returns a list containing all the sentences in the dataset

        return list(map(lambda x: x.content, self.dataset))


    def __len__(self):
        # Function called when calling len(dataset)

        return len(self.dataset)
    
    
    def train_test_split(self, **xargs):
        # Splits the dataset into a training and a test set

        if self.tokenize is None:
            raise Exception("You must fit the tokenizer by called the tokenize() method before being able to use this method.")
        
        ds_train, ds_test = train_test_split(self.dataset, **xargs)

        dataset_train = copy.deepcopy(self)
        dataset_test = copy.deepcopy(self)
        
        dataset_train.dataset = ds_train
        dataset_test.dataset = ds_test

        return dataset_train, dataset_test
    

    def save(self, dir_name):
        # Save the current Object into some files for it to be reusable more easily

        if self.tokenizer is None:
            raise Exception('You have to tokenize the dataset first.')
        
        if not os.path.exists(dir_name):
            os.mkdir(dir_name)

        # Save the dataset as a CSV file
        df_dataset = pd.DataFrame(list(map(lambda x: x.getRaw(), self.dataset)))
        df_dataset = df_dataset[["id", "question_id", "content", "polarity", "subjectivity"]]
        
        df_dataset.to_csv(dir_name + '/dataset.csv', index=False)
        
        # Save the questions as a CSV file
        df_questions = pd.DataFrame([{ 'id': x, 'content': self.questions[x] } for x in self.questions])
        df_questions = df_questions[["id", "content"]]
        
        df_questions.to_csv(dir_name + '/questions.csv', index=False)

        # Save the Tokenizer
        pickle.dump(self.tokenizer, open(dir_name + '/tokenizer.pickle', 'wb+'))


    @classmethod
    def load(cls, dir_name):
        # Creates a Dataset object out of a saved one

        # Creating the dataset
        df_dataset = pd.read_csv(dir_name + '/dataset.csv')
        dataset = []

        for line_id,x in df_dataset.iterrows():
            try:
                dataset.append(Entry(x['id'], x['question_id'], x['content'], x['polarity'], x['subjectivity']))
            except:
                raise Exception('Error when reading the line with the line #' + str(line_id) + ' of the file ' + dir_name + '/dataset.csv')
        

        # Creating the questions
        df_questions = pd.read_csv(dir_name + '/questions.csv')
        questions = {}

        for _,x in df_questions.iterrows():
            questions[x['id']] = x['content']
        
        # Retrieving the Tokenizer
        tokenizer = pickle.load(open(dir_name + '/tokenizer.pickle', 'rb'))

        # Instantiation of our object
        ds = cls('', load=False)
        ds.dataset = dataset
        ds.questions = questions
        ds.tokenizer = tokenizer
        ds.total_words = len(tokenizer.word_index) + 1

        return ds