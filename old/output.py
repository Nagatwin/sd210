#% Text generation
common_start = ["ma liberté", "macron démission", "voiture", "je", "j'aimerais", ]

#% take the best word at every step
def gen_next_words(seed, next_words):
    for j in range(next_words):
        token_list = tokenizer.texts_to_sequences([seed.lower()])[0]
        token_list = pad_sequences([token_list], maxlen=max_sequence_len-1, padding='pre')
        predicted = model.predict_classes(token_list, verbose=0)

        output_word = ""
        for word, index in tokenizer.word_index.items():
            if index == predicted:
                output_word = word
                break
        seed += " " + output_word
    return seed

def gen_common(next_words):
    return [gen_next_words(seed, next_words) for seed in common_start]