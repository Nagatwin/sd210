# sd210

# Description de l'avancement
## Probl�matique, auteurs, toussa

# Pr�processing
## Maquer les points
Une premi�re id�e a �t� de cr�er un token pour les points (on a prit le mot clef end pour �a). 
Cela permet au mod�le d'apprendre o� sont situ�es les fins de phrases et �vite qu'il apprenne des encha�nements de mots non coh�rents (le dernier mot de l phrase pr�c�dente avec le premier de la suivante)

Pour tester, on a fait tourner le mod�le sur un petit dataset pendant ~200 �poques. On obtient le r�sultat suivant :

>ma libert� de la voiture � la campagne de la voiture � la voiture � la voiture � la voiture � la voiture � la voiture � la voiture � la pas de la voiture � la voiture � la voiture � la pas de la voiture � la pas de la voiture � la pas de la voiture � la pas de la plan�te end

>macron d�mission pollution de l'air et les d�r�glements climatiques et la pollution de l'air et les d�r�glements climatiques et la pollution de l'air et les d�r�glements climatiques et la mont�e de l'air et les d�r�glements climatiques et la mont�e de l'air et les d�r�glements climatiques et la mont�e de l'air et les d�r�glements climatiques et la mont�e de l'air et les d�r�glements climatiques et la mont�e de l'air et les d�r�glements climatiques et la mont�e de l'air et les d�r�glements climatiques et la mont�e de l'air et les d�r�glements climatiques et la mont�e de l'air et les d�r�glements climatiques et la mont�e

>voiture est li� et un probl�me en commun � la voiture � la km de la voiture � la voiture � la voiture � la pas de la voiture � la voiture � la voiture � la km de la plan�te end

>je ne peux pas de la voiture est important end

>j'aimerais ne peux pas un probl�me en commun � la demande de la voiture � la voiture � la voiture � la km de la plan�te end
�olienne un r�chauffement climatique et la pollution de l'air et les d�r�glements climatiques et la pollution de l'air et les d�r�glements climatiques et la pollution de l'air et les d�r�glements climatiques et la mont�e de l'air et les d�r�glements climatiques et la mont�e de l'air et les d�r�glements climatiques et la mont�e de l'air et les d�r�glements climatiques et la mont�e de l'air et les d�r�glements climatiques et la mont�e de l'air et les d�r�glements climatiques et la mont�e de l'air et les eaux de la mont�e de la biodiversit� et les probl�mes sont sont disparitions de l'air et les maux


Toutefois nous avons beaucoup trop de tokens, pour les r�duire il y a plusieurs solutions :

* r�duire le dataset $\rightarrow$ dommage, �a nous fait perdre des donn�es et ce n'est pas le but
* r�duire le nombre de mots diff�rents dans le dataset

Pour �a nous avons proc�d� en plusieurs �tapes:

## Remplacer les abr�viations
Nous avons remplac� certaines abr�viations (par exemple, "qu'il") afin de r�duire le nombre de tokens diff�rents.
Ainsi, "qu'il" $\rightarrow$ "que il", "l'air" $\rightarrow$ "le air".

Cela rend les phrases fausses orthographiquement mais facilite le travail du mod�le tout en r�duisant le nombre de tokens (cette �tape a quasiment divis� par 2 le nombre de tokens).
On obtient alors les r�sultats suivants :

>ma libert� les personnes de fonds d�placer en que le air la biodiversit� des d�chets et les entreprises en gratuits nucl�aires rcea dans une vraie politique sur le contexte photovolta�ques et par les gros porteurs et une peu qui en europe end une une pays ne en que des probl�mes et budgets � se d�placer en compte des constructions et

>macron d�mission en pleine campagne il faudrait que je d�m�nage end approche end

>voiture �lectrique ou quadricycle lourd ou l�ger �lectrique end disparitions de c�tes

>je ne peux pas de structure politique pour r�soudre les probl�mes mondiaux pour moi � le environnement end

>je aimerais a pas de solution sauf que les transports en commun car ils sont tous les domaines de se sur les autres end

>�olienne je ne ai pas de solution solution envisageable � mon �ge avec je avons end

## Enlever les mots peu pr�sents dans le dataset

Certains mots sont rares dans le dataset : souvent les fautes d'orthographes et certains mots qui n'ont rien a voir avec le sujet.
Dans la mesure ou ils ne seront jamais pr�dits, nous les avons supprim�s du dataset, apr�s tests le r�sultat reste sensiblement le m�me que dans le cas pr�c�dent, mais plus rapide � entra�ner car moins de tokens.

# L'analyse de sentiment
Pour pouvoir g�n�rer des phrases de mani�re plus pr�cise, nous avons d�cid� de rajouter des tags sur les entr�es : la polarit� (si la r�ponse est positive ou n�gative) et la subjectivit� (si la r�ponse est objective ou non).

Ces composantes ont �t� calcul�es pour chaque entr�e du dataset, avant pr�processing, � l'aide du module textblob.

On a ensuite ajout� au mod�le deux entr�es pour ces deux valeurs.

# Les question_id ?

# La classe dataset

# Le generator

# Le mod�le

# Traduction + autre mod�le

# La generation de texte
Pour g�n�rer le texte apr�s train, la premi�re id�e �tait de prendre le meilleur mot � chaque fois en partant d'une suite de mots. On pouvait alors pr�ciser la polarit�, subjectivit� et l'id de la question si d�sir�.

## Recherche dans un arbre

Toutefois, ce genre de g�n�ration avait tendance � proposer des phrases qui se r�p�taient. Pour �viter cela, nous avons fait une structure d'arbre et plusieurs fonctions de recherche :

* Une **recherche naive**, qui prend en compte la feuille de l'arbre avec le meilleur score et qui approfondit tant que on a pas atteint le nombre de mots donn�

* Une **recherche par faisceau**, qui fait la m�me chose en consid�rant les k meilleurs fils � chaque fois

* Une **recherche par faisceau �tag�e**, qui se comporte comme une recherche par faisceau mais ne revient pas en arri�re en profondeur. Cela a l'avantage d'�tre beaucoup plus rapide � calculer, sans ralentir l'algorithme.

Il faut savoir que nous avons plus de 1500 tokens. Dans le pire cas (souvent la recherche naive ou par faisceau), nous pouvons atteindre $nombre_tokens^{profondeur}$ �tapes, o� profondeur est le nombre de mots d�sir�. Cela est beaucoup trop long.

La **recherche par faisceau �tag�e** est celle qui a �t� retenue pour faire un compromis entre temps de r�ponse et pr�cision du r�sultat.

## La fonction d'�valuation
Pour donner un score aux diff�rentes suites de mots, on a du d�finir une fonction de score. Pour cela nous avions acc�s � la suite de mots, les param�tres (subjectivit�, ...) mais aussi � la probabilit� renvoy�e par le mod�le.

Pour �viter les r�p�titions, le score est alors la probabilit� du mot, p�nalis�e par les r�p�titions dans la phrase g�n�r�e.


# R�sultats/Conclusion