import numpy as np
from keras.utils import to_categorical
from keras.models import Sequential
from keras.layers import GRU, Dense, CuDNNGRU, Embedding
import os, sys
import keras.utils as ku
from keras.preprocessing.sequence import pad_sequences
from research import Tree

os.environ["CUDA_VISIBLE_DEVICES"]="0"

from dataset import Dataset
from generator import Generator


dataset = Dataset.load('model')
tokenizer = dataset.tokenizer

#% Hyper parameters
total_words = dataset.total_words
max_sequence_len = 100
batch_size = 128

#% Creating the model
model = Sequential()
model.add(Embedding(total_words, 10, input_length=max_sequence_len+2))
model.add(CuDNNGRU(256))
model.add(Dense(total_words, activation='softmax'))

#% Loading the model
model.load_weights('weights_model2.h5')

#% Retrieving the given parameters
question_id = 1
polarity = 0
subjectivity = 0.5

next_words = 50
search_size = 5

if len(sys.argv) >= 2:
    question_id = int(sys.argv[1])

if len(sys.argv) >= 3:
    polarity = float(sys.argv[2])

if len(sys.argv) >= 4:
    subjectivity = float(sys.argv[3])

print("\nType the beginning of any sentence: ")
seed = input()

while len(seed) > 0:
    print(Tree(seed, polarity, subjectivity, question_id).depth_based_beam_search(model, tokenizer, max_sequence_len, next_words, search_size)[0].replace(' end ', '.'))

    print("\nType the beginning of any sentence: ")
    seed = input()