import threading
import numpy as np
from keras.preprocessing.sequence import pad_sequences
from keras.utils import to_categorical
from keras.utils import Sequence

class Generator(Sequence):
    def __init__(self, dataset, batch_size, max_sequence_len=100):
        # dataset: a Dataset object
        # batch_size: the size of the batches
        # max_sequence_len: the size of the generated sentences

        self.dataset = dataset
        self.tokenizer = self.dataset.tokenizer
        self.total_words = self.dataset.total_words
        self.batch_size = batch_size
        self.max_sequence_len = max_sequence_len
    
    def __len__(self):
        # Returns the total amount of available non-randomized batch

        return int(np.ceil(len(self.dataset) / self.batch_size))
    
    def __getitem__(self, idx):
        # Returns the i-th non-randomized batch

        batch = self.dataset.dataset[idx * self.batch_size:(idx + 1) * self.batch_size]
        
        return self.getAttributes(batch)
    
    def __iter__(self):
        # Creates what's returned when iterating over the object

        while True:
            batch = self.dataset.getBatch(self.batch_size)
            
            yield self.getAttributes(batch)

    def getAttributes(self, batch):
        # Creates the subsentences out of a given batch and the associated labels
        # Also concatenate the question ID to the subsentences
        
        input_sequences = None
        question_ids = []
        sentiments = []

        for data in batch:
            subsequences = data.getSubSequences(self.tokenizer, self.max_sequence_len)
            question_id = data.question_id

            if input_sequences is None:
                input_sequences = np.array(subsequences)
            else:
                input_sequences = np.concatenate((input_sequences, subsequences))
            
            question_ids = question_ids + [question_id] * len(subsequences)
            sentiments = sentiments + [[data.polarity, data.subjectivity]] * len(subsequences)

        predictors, label = input_sequences[:,:-1], input_sequences[:,-1]
        label = to_categorical(label, num_classes=self.total_words)

        # We add the question ID to the input
        predictors = np.concatenate((sentiments, np.expand_dims(question_ids, axis=1), predictors), axis=1)

        return predictors, label