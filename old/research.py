#% Tree for generation

#% WARNING
#% No pruning should be done, search methods are not guaranteed to give exact optimum at given depth
from keras.preprocessing.sequence import pad_sequences
import numpy as np

class Node:
  def __init__(self, seed, polarity, subjectivity, question_id, proba, depth):
    # seed: seed text
    # polarity, subjectivity, question_id: explicit
    # proba: proba of this node being the correct one according to the model
    # depth: depth of node
    self.seed = seed
    self.proba = proba
    self.polarity = polarity
    self.subjectivity = subjectivity
    self.question_id = question_id
    self.depth = depth
    
    # children of the node
    self.children = []
  
  #% generate childs for this node
  def go_deeper(self, model, tokenizer, max_sequence_len):
    # Build token
    token_list = tokenizer.texts_to_sequences([self.seed.lower()])[0]
    token_list = pad_sequences([token_list], maxlen=max_sequence_len-1, padding='pre')
    add_token = np.array([[self.polarity, self.subjectivity, self.question_id]])
    token = np.concatenate((add_token,token_list), axis=1)
    
    # Predict
    predicted = model.predict(token, verbose=0)
    sorted_predictions = [[x.argsort()[:][::-1]] for x in predicted]
    output_words = tokenizer.sequences_to_texts(sorted_predictions[0])[0].split()
    
    # Create new childrens
    for i in range(len(output_words)):
      word = output_words[i]
      self.children.append(Node(self.seed + " " + word, self.polarity, self.subjectivity, self.question_id, predicted[0][sorted_predictions[0][0][i]], self.depth + 1))
  
  #% give a score to a node
  def eval(self, tokenizer):
    # return self.proba
    # Avoid repetitions
    token_list = tokenizer.texts_to_sequences([self.seed.lower()])[0]
    if len(token_list) > 0:
        return self.proba + len(list(set(token_list))) / len(token_list)
    else:
        return self.proba
 
  #% Get node string representation
  def __repr__(self):
    args = self.__dict__.copy()
    args['children'] = len(self.children)
    return "Node : " + str(args)
  
  #% get the whole list of children
  def build_leaf_list(self):
    if self.children == []:
      return [self]
    else:
      res = []
      for child in self.children:
        res += child.build_leaf_list()
      return res
  
  #% Get the list of nodes at depth depth
  def get_nodes_at_depth(self, depth):
    if self.depth == depth:
      return [self]
    elif self.children == []:
      return []
    else:
      res = []
      for child in self.children:
        res += child.get_nodes_at_depth(depth)
      return res
      
#% Tree class
class Tree(Node):
  def __init__(self, seed, polarity, subjectivity, question_id):
    Node.__init__(self, seed, polarity, subjectivity, question_id, 1, 0)
    self.best = self
  
  #% Search on best child
  def search_step(self, model, tokenizer):
    leaf_list = sorted(self.build_leaf_list(), key=lambda node: -node.eval(tokenizer))
    for leaf in leaf_list[:min(1, len(leaf_list))]:
      leaf.go_deeper(model, tokenizer, max_sequence_len)
    self.best = sorted(self.build_leaf_list(), key=lambda node: -node.eval(tokenizer))[0]
    
  #% Basic search, can be slow
  def search(self, model, tokenizer, max_sequence_len, depth):
    while(self.best.depth != depth):
      self.search_step(model, tokenizer, max_sequence_len)
    return self.best.seed, self.best.eval(tokenizer)
  
  #% Search on best child
  def beam_step(self, model, tokenizer, max_sequence_len, beam_size):
    leaf_list = sorted(self.build_leaf_list(), key=lambda node: -node.eval(tokenizer))
    for leaf in leaf_list[:min(beam_size, len(leaf_list))]:
      leaf.go_deeper(model, tokenizer, max_sequence_len)
    self.best = sorted(self.build_leaf_list(), key=lambda node: -node.eval(tokenizer))[0]
    
  #% Basic search, can be slow
  def beam_search(self, model, tokenizer, max_sequence_len, depth, beam_size):
    while(self.best.depth != depth):
      self.beam_step(model, tokenizer, max_sequence_len, beam_size)
    return self.best.seed, self.best.eval(tokenizer)
  
  #% Beam based search at each step
  def depth_based_beam_search(self, model, tokenizer, max_sequence_len, depth, beam_size):
    for step in range(depth + 1):
      leaf_list = sorted(self.get_nodes_at_depth(step), key=lambda node: -node.eval(tokenizer))
      for leaf in leaf_list[:min(beam_size, len(leaf_list))]:
        leaf.go_deeper(model, tokenizer, max_sequence_len)
      self.best = sorted(self.get_nodes_at_depth(step + 1), key=lambda node: -node.eval(tokenizer))[0]
    return self.best.seed, self.best.eval(tokenizer)

#% example:
# from research import Tree
# Tree with seed "je"
# using polarity -1, subjectivity 0.3 and question 3
#t = Tree("je", -1, 0.3, 3)
# Search next 10 words, considering the 5 bests at each step
#t.depth_based_beam_search(model, tokenizer, 10, 5)